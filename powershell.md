# PowerShell

## Table of contents

[[_TOC_]]

<!--
* [Basics](#intro)
  * [Cmdlets, Aliases and Help](#cmdlets)
  * [Execution Policies](#exe)
  * [Drives, Profiles and Namespaces](#drives)
  * [Windows Update](#update)
  * [Install PowerShell Core](#core)
  * [Install/Upgrade Additional Software](#sw)
* [Objects and Pipeline](#object)
  * [Get-Member](#gm)
  * [Where-Object, Where, ?](#where)
  * [ForEach-Object, foreach, %](#foreach)
  * [Sort-Object, Select-Object](#sort)
  * [Format-Table, Format-List](#format)
  * [Measure-Object](#measure)
  * [Variable, Array and Hashtable](#vars)
* [Remoting](#remote)
  * [Copying files with and without Shares](#copy)
* [Scripts](#scripts)
* [Accessing .NET](#dotnet)
* [System Info Cmdlets](#sys)
* [Networking Cmdlets](#network)
* [Additional Resources](#resources)
-->


Windows have GUI (the [Windows
Shell](https://en.wikipedia.org/wiki/Windows_shell)), cmd (the old
DOS-shell) and PowerShell (cmd and PowerShell is united in the [Windows
Terminal](https://github.com/microsoft/terminal) but it is as of Dec 2019
only available for Windows 10).

How to copy and paste in the Windows command line (ctrl-c does not copy, it
terminates your running command :).





## <a name="intro"></a>Basics

Note: for this tutorial you sometimes need to run PowerShell as
administrator. If you are using a Windows with a different keyboard layout
than what you are used to (e.g. if you have downloaded from [Get a Windows 10 development environment](https://developer.microsoft.com/en-us/windows/downloads/virtual-machines)) 
```powershell
Get-WinUserLanguageList
Set-WinUserLanguageList -LanguageList nb-NO
```


### <a name="cmdlets"></a>Cmdlets, Aliases and Help

PowerShell commands are called *cmdlets* (pronounced “commandlets”) and
have the syntax `verb-noun`, e.g. `Write-Output`. Fortunately most of
the cmdlets have aliases corresponding to the commands you might know
from DOS (cmd.exe) or Unix/Linux. In addition there is also a short
PowerShell alias to most cmdlets. To find the cmdlet to a command you
know from before you can use the cmdlet `Get-Alias`:
```powershell
# is there a cmdlet corresponding to Unix/Linux ls?
Get-Alias ls
# are there many aliases to Get-ChildItem?
Get-Alias -Definition Get-ChildItem
# list all aliases
Get-Alias
# or list them from the "alias drive" (similar to your C:\ drive)
Get-ChildItem Alias:\
``` 
Note: Aliases are nice, but get used to using the full cmdlet names,
especially in your scripts. This makes your scripts more readable and
easier for others to understand.

These cmdlets has several parameters/options that are specific to each one, but there are
also a set of [common
parameters](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_commonparameters?view=powershell-6#long-description). Note
particularly the options `-Verbose`, `-ErrorAction SilentlyContinue` and
`-WhatIf`.

> Try `Get-Service | Stop-Service -WhatIf`. Try `Get-ChildItem -Recurse
> C:\Windows\System32\wbem\MO*` with and without the option `-ErrorAction
> SilentlyContinue`. 

To get help with the cmdlets, use the cmdlet `Get-Help`, e.g. `Get-Help
Write-Output | more`. A nice feature is that you can view the help page in
your browser (on the internet) by adding the parameter `-Online`,
e.g. `Get-Help Write-Output -Online`. If you don't want to go online, you
can use `Get-Help Write-Output -Examples` or `Get-Help Write-Output -Full`.

Note that you can use TAB-completion on both commands and parameters.

> Use `Get-Help Select-String -online` and find the example code for "Find
> a string in subdirectories". Try it. Did you get an access error (red
> text)? If so, repeat the command with the option `-ErrorAction
> SilentlyContinue` added to `Get-ChildItem`. Searching file systems is
> slow, how slow? Repeat the command but put braces around it and prepend
> it with the cmdlet `Measure-Command`.

### <a name="exe"></a>Execution Policies

PowerShell has [Execution
Policies](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_execution_policies?view=powershell-6#long-description). We
set this to
[RemoteSigned](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_execution_policies?view=powershell-6#remotesigned)
to require scripts downloaded from the Internet to be digitally signed by a
trusted publisher. If we have downloaded a script that is not digitally
signed by a trusted publisher, we can still execute it if we manually
`Unblock-File`.

### <a name="drives"></a>Drives, Profiles and Namespaces

A PowerShell drive is a data store which you can access like a file
system. This might seem a bit strange at first, but it is very practical
and allows us to reuse the same set of commands for accessing different
kinds of data.

> `Get-PSDrive`, navigate to all drives you did not know about, and do `Get-ChildItem`
> and/or `Get-ChildItem -Recurse` to explore what is there.

PowerShell has namespaces. Sometimes you have to specify namespace and
sometimes you don't
```powershell
$a = 'heisan'
Write-Output $a
Write-Output $variable:a
cd $env:homepath
$env:homepath
$homepath
$home
$env:home
$profile
Get-Content $profile
```

### <a name="update"></a>Windows Update

```powershell
# PowerShell -> Run as administrator 

# Allow for execution of scripts and loading of configuration files 
# that we create locally
Set-ExecutionPolicy RemoteSigned

# Always update...
Install-Module -Name PSWindowsUpdate
Get-WUInstall -Verbose
Get-WUInstall -Install
```
> `Get-WUInstall` by default contacts "Windows update server" which only
> covers updates for the operating system. Can you use `Get-WUInstall` to
> contact "Microsoft Update server" (updates for other Microsoft software) instead?

### <a name="core"></a>Install PowerShell Core

Windows 10 and Windows Server 2019 ship with Windows PowerShell 5.1 as the
default version. PowerShell Core is a new edition/generation of PowerShell
from version 6 that is cross platform (Windows, Mac, Linux). PowerShell
core is not an upgrade of version 5.1, it is a separate
installation. PowerShell 7 (an upgrade to 6) is probably available when you
read this.

```powershell
# Check PowerShell version
$PSversionTable
# Install latest PowerShell (core)
iex "& { $(irm https://aka.ms/install-powershell.ps1) } -UseMSI"
# (or see choco command below)
```
### <a name="sw"></a>Install/Upgrade Additional Software

Install Chocolatey package manager according to [Installing
Chocolatey](https://chocolatey.org/install). Chocolatey is very cool, but
make sure you know which risk you are taking. Read [Rigorous Moderation
Process for Community
Packages](https://chocolatey.org/docs/security#rigorous-moderation-process-for-community-packages).

```powershell
choco install nano
choco install powershell-core
choco install openssl
choco outdated
choco upgrade all
```
> Use `choco` to install the package `autoruns`. Launch `autoruns` after
> installation, what is the purpose of this software? What is Sysinternals?





## <a name="object"></a>Objects and Pipeline

The "Power" in PowerShell comes from piping objects instead of just a
bytestream. E.g. if you want to see a list of network shares you can use
the old-style `C:\Windows\System32\net.exe` program or the new PowerShell
cmdlet `Get-SmbShare`
```powershell
net use
Get-SmbShare
net use | Select-String windows
net use | Where Path -match 'windows'
Get-SmbShare | Select-String windows
Get-SmbShare | Where Path -match 'windows'
```

### <a name="gm"></a>Get-Member

When piping objects you can use `Get-Member` to see which properties and
methods the object contains.
```powershell
net use | Get-Member
Get-SmbShare | Get-Member
Invoke-WebRequest https://raw.githubusercontent.com/dwyl/english-words/master/words.txt -OutFile words
Get-ChildItem .\words
Get-ChildItem .\words | Get-Member
(Get-ChildItem .\words).Length
$fs = (Get-ChildItem .\words).Length
$fs | Get-Member
$fs.GetType()
$fs*2
```
> Do `$name = 'mysil'`. Use the properties and methods of this object to
> * find out how many characters the string contains
> * print the string in upper case

### <a name="where"></a>Where-Object, Where, ?

Objects in the pipeline can be addressed with the special variable `$_` and
`Where-Object` is used to filter objects similar to Unix/Linux `grep`
```powershell
Get-Service
Get-Service | Where-Object {$_.Status -eq 'Running'}
```
> Use `Get-Process` and `Where-Object` to
> * list all processes with the property Name equals powershell
> * list all processes with the property WorkingSet greater than 10MB
> * use Get-Member to find a property that will show you the full path to
>   the executable file powershell.exe (for the powershell process object)

Note: you should for efficiency reasons (and thereby climate change
reasons) filter objects as far left as you can, e.g. do 
`Get-Process -Name notepad` instead of
`Get-Process | Where-Object {$_.Name -eq 'notepad'}`

### <a name="foreach"></a>ForEach-Object, foreach, %

You use `Where-Object` for filtering objects in the pipeline. You use
`ForEach-Object` when you want to do something (with some flexibility) to
each object in the pipeline.
```powershell
foreach ($i in 1..4) {notepad}
Get-Process notepad | ForEach-Object {Write-Output "Terminating Notepad with id $($_.id)";$_.Kill()}
```
Note: if you just wanted to stop all notepad-processes you could simply
pipe to `Stop-Process`.

### <a name="sort"></a>Sort-Object, Select-Object 

```powershell
Get-ChildItem C:\Windows\System32\ | Where-Object {$_.LastAccessTime -gt (Get-Date).AddMinutes(-30)} | Sort-Object -Property Length -Descending
Get-ChildItem C:\Windows\System32\ | Where-Object {$_.LastAccessTime -gt (Get-Date).AddMinutes(-30)} | Sort-Object -Property Length | Select-Object -Last 10
```
> Do `cd $env:homepath`. List all files recursively in your home directory
> where the filename matches the regular expression `'.*\.[tpl].*'`. Sort
> the output on the property LastAccessTime 

### <a name="format"></a>Format-Table, Format-List

```powershell
Get-ChildItem -File C:\Windows\ | Format-List -Property Name,Length,LastAccessTime
Get-ChildItem -File C:\Windows\ | Format-Table -Property Name,Length,LastAccessTime
```
You can also send output from the pipeline to other places than the
terminal window
```powershell
Get-ChildItem -File b* | Out-File b.dat          # is the same as > b.dat
Get-ChildItem -File b* | Out-GridView
Get-ChildItem -File b* | clip                    # send it to the clipboard
Get-ChildItem -File b* | Export-Csv $home\a.csv
```
> TAB through all `ConvertTo-` cmdlets

### <a name="measure"></a>Measure-Object

```powershell
Get-ChildItem -File C:\Windows\System32\ | Measure-Object
Get-ChildItem -File C:\Windows\System32\ | Measure-Object -Property Length -Sum -Average
```

### <a name="vars"></a>Variable, Array and Hashtable

A variable can represent any kind of object, and a variable can be grouped
into an array or a hashtable. In an array, each element has a numeric
index. In a hashtable each element has a key as index (in other words, a
hashtable is a collection of key-value pairs).
```powershell
$a = 'hei'
$a.gettype()
$a = Get-ChildItem C:\Windows\system.ini
$a.gettype()
$a = @('frodeh','kelly')
$a.gettype()
$a[1]
$a
$a = @{'frodeh'='Frode Haug';'kelly'='Jia-Chun Lin'}
$a.gettype()
$a[1]
$a['kelly']
$a
$a.length
$a.keys
```
Useful Hashtable
```powershell
Get-ChildItem C:\Windows\System32\ntoskrnl.exe | Select-Object -Property Name,@{name='ReadableSize';expression={$_.Length/1MB}}
```
> Using the hashtable `$a` above (the last example), write a command
> line which will output "User frodeh has real name Frode Haug"





## <a name="remote"></a>Remoting

With PowerShell remoting you can execute commands or login (create a
session) on remote hosts [using either the WSMan or SSH
protocols](https://docs.microsoft.com/en-us/powershell/scripting/learn/remoting/running-remote-commands). We
will only focus on WSMan between Windows hosts for now.

Remoting is enabled by default on Windows Server (if you want to connect to
a Windows 10 host you have to enable remoting with `Enable-PSRemoting`).

> Log in to `srv1` and try the commands on `dc1` as remote host using user
> `RESKIT\Admin` (password from the Admin user on `dc1`) in the article [About
> Remote](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_remote)

Users has to be in Builtin/Administrators group to be able to remote on a
domain controller (this replaces the local adminsitrator group on domain
controllers).

If remoting does not work, or if you are trying to set up remoting on hosts
that are not part of the same domain (or not joined to any domain), here
are som useful commands
```powershell
Get-Help about_remote_requirements -Online
Get-Service WinRM                          # Is the service running?
Get-NetTCPConnection -LocalPort 5985,5986  # Listening service on the right ports?
Test-WSMan
Test-WSMan -ComputerName srv2 -Authentication Default
Get-Item WSMan:\localhost\Client\TrustedHosts # Any trusted hosts?
# if Enable-PSRemoting -Force does not work try
winrm quickconfig
```
### <a name="copy"></a>Copying files with and without Shares

In the Linux world you always copy files with `scp` and you can use this
from Windows to a Linux host as well but the Windows-way of copying
files between hosts is using existing shares (this only works for writing
to domain controllers)
```powershell
Write-Output heisan > mysil.txt
Copy-Item mysil.txt \\dc1\C$\users\admin
Get-ChildItem \users\admin
Enter-PSSession dc1
Get-SmbShare
Get-ChildItem \users\admin
```
or creating temporary shares (a share is a network file system, in Windows
this is SMB (Server Message Block) sometimes called CIFS (Common Internet
File System))
```powershell
Enter-PSSession srv1
New-SmbShare -Name "admindocs$" -Path "C:\users/admin/documents" -Temporary -FullAccess "RESKIT\Administrator"
exit
Copy-Item mysil.txt \\srv1\admindocs$
Enter-PSSession srv1
Get-ChildItem C:\Users\Admin\Documents
Remove-SmbShare admindocs$
exit
``` 
> Do the `New-SmbShare` cmdlet above, but put all the arguments in a
> hashtable (this is called [splatting](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_splatting) and is much used in the textbook)

It is possible to do something similar to scp in PowerShell by establishing
a remote session and then use `Copy-Item` with the `-ToSession` option
```powershell
$sess = New-PSSession srv1
Copy-Item .\mysil -ToSession $sess C:\users\admin
Get-PSSession
Remove-PSSession $sess
Get-PSSession
```


## <a name="scripts"></a>Scripts

Check code quality with [PSScriptanalyzer](https://github.com/PowerShell/PSScriptAnalyzer) which is a static code analyzer for PowerShell. For bigger projects one should look into [Pester](https://github.com/pester/Pester).
```powershell
Install-Module -Name PSScriptAnalyzer
# use it on script a.ps1
Invoke-ScriptAnalyzer a.ps1
```
Sometimes you will see `&` as prefix to scripts or commands when you
TAB-complete. `&` is the call-operator, browse [about
Operators](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_operators)
to learn more.





## <a name="dotnet"></a>Accessing .NET

Sometimes you will not find a cmdlet that does exactly what you need to
do. In those situations it is useful to know that through PowerShell you
have access to everything in .NET (PowerShell is built on top of
.NET). E.g. in the textbook Thomas Lee in one recipe writes *PowerShell's
certificate provider does not support copying a certificate into the root
CA store.  You can overcome this limitation by dipping down into the .NET
framework as shown in step 7*

You can access .NET-objects by starting a command with a bracket
```powershell
[math]::PI
2.3*4.5
[math]::Round(2.3*4.5)
Get-Date
[datetime]::Now
[datetime]::UtcNow
```
> Try `[System` and TAB your way through possible .NET-objects.

In PowerShell you use the `.`-operator when you access the properties and
methods of an object, e.g. `(Get-Process powershell).WorkingSet`. The
`::`-operator is used to access static properties and methods (properties
and methods that does not
belong to a specific instance of a class), you can view these with
`Get-Member` as well
```powershell
(Get-Date).GetType()
Get-Date | Get-Member
Get-Date | Get-Member -Static
[datetime] | Get-Member
[datetime] | Get-Member -Static
```
> Do `Add-Type -AssemblyName 'System.Web'` and then REMEMBER TO ALWAYS
> GENERATE PASSWORDS THIS WAY (similar to pwgen on Linux) `[System.Web.Security.Membership]::GeneratePassword(16,0)`
> (or simpler, but with code from third-party, `Install-Module MlkPwgen`
> and do `New-Password -Length 16`)


## <a name="sys"></a>System Info Cmdlets

```powershell
Get-Process
Get-Service
Get-ComputerInfo | Format-Table -AutoSize -Property WindowsVersion,CsPartOfDomain
Get-CimClass Win32*
Get-CimInstance Win32_ComputerSystem
```





## <a name="network"></a>Networking Cmdlets

Useful networking cmdlets are

```powershell
Test-NetConnection
Get-NetAdapter
Get-NetIPConfiguration
Get-NetTCPConnection
Get-NetRoute
Resolve-DnsName
Get-DnsClient
Get-DnsClientServerAddress
Get-DnsClientGlobalSetting
Clear-DnsClientCache
```
These also have corresponding "Set-" cmdlets, and some useful pipelines are
```powershell
Set-DnsClientGlobalSetting -SuffixSearchList @("node.consul")
Get-NetAdapter | Set-DnsClient -ConnectionSpecificSuffix "node.consul"
Get-NetAdapter | Get-NetIPInterface -Addressfamily IPv4 | Set-NetIPInterface -DHCP Enabled
```





## <a name="resources"></a>Additional Resources

What seems to be one of the best resources for learning PowerShell is the
book [Don Jones and Jeffery Hicks, Learn Windows PowerShell in a Month of
Lunches](https://www.manning.com/books/learn-windows-powershell-in-a-month-of-lunches-third-edition)
which also have a accompanying [video
series](https://www.youtube.com/playlist?list=PL6D474E721138865A). A newer
and more comprehensive video series is [PowerShell Master
Class](https://www.youtube.com/playlist?list=PLlVtbbG169nFq_hR7FcMYg32xsSAObuq8). An
excellent resource for PowerShell remoting is [Secrets of PowerShell
Remoting](https://leanpub.com/secretsofpowershellremoting/read).

