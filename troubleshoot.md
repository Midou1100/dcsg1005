[[_TOC_]]

# Feilsøke stacken

## 1. Sjekk DC1

Logg inn på dc1, sjekk at puppet kjører og om AD er ferdiginstallert
```
PS> Get-Service puppet
Status   Name               DisplayName
------   ----               -----------
Running  puppet             Puppet Agent
```
HVIS STATUS ER STOPPED, GJØR `Start-Service puppet` OG VENT 10 MINUTTER
```
PS> Get-ADRootDSE
configurationNamingContext    : CN=Configuration,DC=reskit,DC=org
currentTime                   : 24.04.2020 09:58:12
defaultNamingContext          : DC=reskit,DC=org
dnsHostName                   : dc1.reskit.org
...osv...
```
HVIS IKKE DET SER SLIK UT OG DET ER OVER TO TIMER SIDEN DU STARTET STACKEN
OG VI IKKE ER MIDT I PATCH TUESDAY, `Restart-Computer` OG VENT 10 MINUTTER


## 2. Har alle registert seg selv i DNS via consul?

Hvis du ikke får logget inn som `RESKIT\Administrator` på en av de andre
host'ene, så logg inn som lokal Admin og sjekk at alle er `alive` i
consul-clusteret
```
PS> & 'C:\ProgramData\consul\consul.exe' members
Node     Address               Status  Type    Build  Protocol  DC    Segment
dir      192.168.180.115:8301  alive   server  1.6.2  2         ntnu  <all>
manager  192.168.180.133:8301  alive   server  1.6.2  2         ntnu  <all>
mon      192.168.180.151:8301  alive   server  1.6.2  2         ntnu  <all>
cl1      192.168.180.110:8301  alive   client  1.6.2  2         ntnu  <default>
dc1      192.168.180.164:8301  alive   client  1.6.2  2         ntnu  <default>
dc2      192.168.180.181:8301  alive   client  1.6.2  2         ntnu  <default>
srv1     192.168.180.161:8301  alive   client  1.6.2  2         ntnu  <default>
srv2     192.168.180.196:8301  alive   client  1.6.2  2         ntnu  <default>
```
HVIS NOEN IKKE ER TILSTEDE ELLER ALIVE, SÅ LOGG INN SOM LOKAL ADMIN PÅ DEN
HOST'EN OG SJEKK AT PUPPET KJØRER (EVN `Start-Service puppet`, DET ER PUPPET
SOM SØRGER FOR AT CONSUL KJØRER)

## 3. Bruker alle DC1 som sin DNS-server? 

Logg inn som lokal Admin på en av de andre host'ene
```
PS> (Resolve-DnsName dc1).Name
dc1.reskit.org
```
HVIS DC1 SVARER SOM dc1.node.consul SÅ LOGG INN PÅ MANAGER, GJØR `sudo -i`
OG DERETTER `netplan apply`, OG VENT 10 MINUTTER TIL PUPPET HAR FIKSET DNS
PÅ HOST'ene

(merk: dette er årsaken til alle domene-relaterte problemer, dvs
f.eks. hvis SharpHound sier noe om "missing domain context" el.l.,
DNS-oppslag av dc1 må gi `dc1.reskit.org` som svar og ikke
`dc1.node.consul`)
