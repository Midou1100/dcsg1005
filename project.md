# Project Title

## Table of contents
* [The Hard Facts](#intro)
* [Report Structure](#struct)
* [How do I get help?](#help)

This is a security project: **Remember ethics and legal regulations. If you
discover a vulnerability, DO NOT ATTEMPT TO EXPLOIT IT OUTSIDE YOUR LAB
ENVIRONMENT. Attacking others can give you big trouble later in your
career.**

ALSO: **BEFORE YOU DO ANY ATTACK ON YOUR HOSTS IN SKYHIGH, NOTIFY ERIK AND
JANNIS AND WAIT FOR THEIR APPROVAL** (SkyHiGh is a shared resource, so we
have to be careful with anything that can affect others).

Your project task is to research an attack on "a component" in a Windows
Infrastructure. "A component" can be anything we have covered in this
course, or some other Windows-based important software that can be
attacked. You are going to attempt an attack, check if it was successful or
not, implement or describe a defense mechanism that would stop or detect
the attack. See examples of relevant source to start your project in the
Security chapter of the
[compendia](https://gitlab.com/erikhje/dcsg1005/-/blob/master/compendia.pdf).

## <a name="intro"></a>The Hard Facts
* Join together to form project groups of max three people in each group
  (you can also be just one if you prefer to work alone). Each student
  should spend 25 hours on this project.
* You have to write down the hours you (each student) spend and what you
  did in those hours: a very brief log of your time spent. This should be
  included in your report as an appendix. Imagine that you are a consultant
  doing pentesting and security analysis, and the customer will only pay
  you for 25 hours, how efficient can you be? how much can you automate and
  document? 
* You will be graded (in 2020 this will only be pass or fail) based on
  * 40% how hard you have worked
  * 40% how difficult the project is
  * 20% report quality (formalities)
* Write a report. The report must be at least five pages long, but no more
  than ten pages. The report can have additional appendices if you want to
  include something you cannot make room for in the main part of the
  report. The report must be handed in in Inspera in PDF format. It is
  recommended that you write the report using Markdown (can convert to
  docx, upload to Office365 and create PDF) since this is a valuable
  learning experience and something everyone needs to master.
* [YOU HAVE TO WRITE IN YOUR OWN
  WORDS](https://innsida.ntnu.no/wiki/-/wiki/English/Cheating+on+exams). All
  reports will be checked automatically for plagiarism. *If you use text
  from other sources, you have to cite the source and clearly show in the
  text that these are not your own words*. Try to write as scientifically
  correct as you can. Write objectively, do not write like you are telling
  a story: "first we did this, then we did that, ...".
* You have to demonstrate proficiency in PowerShell. This does not mean
  that you have to do the actual attack or defense from PowerShell, but
  your project should contain command lines wherever appropriate. Automate
  as much as you can.

## <a name="struct"></a>Report Structure

1. Short topic intro and why you chose it (Research phase, do not spend
   more than 3-4 hours on this)
1. Prepare the environment (be careful so this does not steal too much
   time)
1. Describe and perform the attack (5-12 hours)
1. How the check if it worked or not (1-3 hours)
1. How to defend (3-8 hours)
1. Self-evaluation (1 hour)
1. Additional resources, references, appendices (including timesheet/log)

Btw, if you write in markdown, you can use pandoc to convert to e.g. docx
(then upload the docx to Office365 and convert to PDF for instance)
```
choco install pandoc
pandoc -s project.md -o project.docx
```

## <a name="help"></a>How do I get help?

Contact Jannis or Erik any time (we might not respond immediately but we
are available in Teams Thursdays 10-14 and almost every other day as
well). Drop us an email if you dont find us in Teams.
